import csv
import re

dnslog = open('dns.log')
traffic = open('hosts.txt')
dnstraffic = csv.reader(dnslog, delimiter="\x09")
unwanted_hosts = 0
existing_unwanted_hosts = 0
reg = re.compile('[^a-zA-Z.]')
hosts=[]

for line in traffic.readlines():
	if (len(line.split(' ')) > 1):
		hosts.append(reg.sub('', line.split(' ')[1]))

for row in dnstraffic:
	try:
		unwanted_hosts+=1
		if row[9] in hosts:
			print(row[9])
			existing_unwanted_hosts+=1
	except IndexError:
		pass

print(str(existing_unwanted_hosts * 100.0 / unwanted_hosts) + "% is unwanted traffic!")

dnslog.close()
traffic.close()